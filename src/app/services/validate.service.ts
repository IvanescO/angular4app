import { Injectable } from '@angular/core';

@Injectable()
export class ValidateService {

  constructor() { }

  validateFirstName(fn) {
    const re = /[A-ZА-Я]{1}[а-яa-z]{2,15}/;
    return re.test(fn);
  }

  validateLastName(ln) {
    const re = /[A-ZА-Я]{1}[а-яa-z]{2,15}/;
    return re.test(ln);
  }

  validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  validatePassword(pass) {
    if (pass.length > 8 && pass.length < 32) {
      return true;
    }
    else
      return false;
  }


}
